import s from "./style.module.css";
import { LazyLoadImage } from "react-lazy-load-image-component";
import "react-lazy-load-image-component/src/effects/blur.css";

const Modal = ({ show, children, closeModal, selectedMovie }) => {
  const backdropPath = `https://image.tmdb.org/t/p/original${selectedMovie?.backdrop_path}`;
  const backdropPlaceholder = `https://image.tmdb.org/t/p/w500${selectedMovie?.backdrop_path}`;

  if (!show) {
    return null;
  }

  return (
    <div className={s.modal_backdrop}>
      <div className={s.modal}>
       
           <LazyLoadImage
          className={s.background_image}
          src={backdropPath}
          threshold='100'
          placeholderSrc={backdropPlaceholder}
          alt="background_poster"
        />

        <button className={s.modal_button} onClick={closeModal}>
          Close
        </button>
        {children}
       

      </div>
    </div>
  );
};

export default Modal;
