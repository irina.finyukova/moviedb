import { useState } from "react";
import s from "./style.module.css";
import Vote from "../Vote/Vote";
import { LazyLoadImage } from "react-lazy-load-image-component";
import "react-lazy-load-image-component/src/effects/blur.css";

const ModalContent = (props) => {
  const [activeTab, setActiveTab] = useState("movie_info");

  const posterPath = `https://image.tmdb.org/t/p/original/${props.selectedMovie.poster_path}`;
  const posterPlaceholder = `https://image.tmdb.org/t/p/w500/${props.selectedMovie.poster_path}`;

  const getGenreNames = (genreIds, allGenres) => {
    return genreIds
      .map((id) => allGenres.find((genre) => genre.id === id)?.name)
      .filter((name) => name)
      .join(", ");
  };

  const genreNames = getGenreNames(props.selectedMovie.genre_ids, props.genres);

  return (
    <div className={s.modal_content}>
      <div className={s.flex_wrapper}>

        <LazyLoadImage
          className={s.poster_image}
          src={posterPath}
          effect="blur"
          threshold="100"
          placeholderSrc={posterPlaceholder}
          alt="poster"
        />

        <div className={s.info_wrapper}>
          <div className={s.tab_buttons}>
            <button
              className={activeTab === "movie_info" ? s.active_tab : s.tab_button}
              onClick={() => setActiveTab("movie_info")}
            >
              Movie info
            </button>
            <button
              className={activeTab === "watch_trailer" ? s.active_tab : s.tab_button}
              onClick={() => setActiveTab("watch_trailer")}
            >
              Watch trailer
            </button>
          </div>

          {activeTab === "movie_info" && (
            <div className={s.info_wrapper}>
              <div className={s.title}>
               
                {props.selectedMovie.title}
                <Vote scores={props.selectedMovie.vote_average} />
              </div>

              <div className={s.additional_info}>
                <div className={s.date}>
                  <span className={s.subtitle}>Release date:</span>
                  {props.selectedMovie.release_date}
                </div>

                <div className={s.language}>
                  <span className={s.subtitle}>Language:</span>
                  {props.selectedMovie.original_language}
                </div>

                <div>
                  <span className={s.subtitle}> Genre:</span> {genreNames}
                </div>
              </div>

              <div className={s.overview}>
                <span className={s.subtitle}> Overview:</span> <br />
                {props.selectedMovie.overview}
              </div>
            </div>
          )}

          {activeTab === "watch_trailer" && (
            <div class={s.video_wrapper}>
              <iframe
                title="Movie video"
                class={s.responsive_iframe}
                src={`https://www.youtube.com/embed/${props.selectedMovieVideoId}`}
                frameBorder="0"
                allowFullScreen
              ></iframe>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default ModalContent;
