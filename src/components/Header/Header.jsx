import s from "./style.module.css";


const Header = (props) => {

  const handleSubmit = (e) => {
    e.preventDefault()
  }

  return (
    <div className={s.search_wrapper}>
      <img className={s.logo} src="../images/Search_logo.png" alt="logo_image" />

      <form className={s.search_form} onSubmit={handleSubmit}>
        <input
          className={s.search_input}
          type="text"
          placeholder="Find a movie"
          onChange={props.handleOnChange}
        />
      </form>
    </div>
  );
};

export default Header;
