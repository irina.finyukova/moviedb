import { useEffect, useState } from "react";
import axios from "axios";
import Movies from "../Movies/Movies";
import s from "./style.module.css";
import Pagination from "../Pagination/Pagination";
import Header from "../Header/Header";
import Loader from "../Loader/Loader";
import GenresSelection from "../GenresSelection/GenresSelection";
import {
  API,
  SEARCH_API,
  GENRES_API,
  getMovieDetailsURL,
  getGenresSelectedURL,
  getScoreSelectedURL,
  getRatingAndGenresURL,
} from "../../const";
import RangeSlider from "../RangeSlider/RangeSlider";

function Main() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [title, setTitle] = useState("");
  const [moviesPerPage] = useState(8);
  const [selectedMovie, setSelectedMovie] = useState(null);
  const [selectedMovieVideoId, setSelectedMovieVideoId] = useState(null);
  const [genres, setGenres] = useState([]);
  const [selectedGenres, setSelectedGenres] = useState([]);
  const [ratingValues, setRatingValues] = useState([0, 10]);


  const indexOfLastPost = currentPage * moviesPerPage;
  const indexOfFirstPost = indexOfLastPost - moviesPerPage;
  const currentMovies = data.slice(indexOfFirstPost, indexOfLastPost);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const getData = async (api) => {
    setLoading(true);
    axios
      .get(api)
      .then((response) => {
        setData(response.data.results);
      })
      .catch((error) => {
        setErrorMessage(error);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    getData(API);
    setTitle("Popular movies");

    const fetchGenres = async () => {
      setLoading(true);
      try {
        const response = await axios.get(GENRES_API);
        setGenres(response.data.genres);
      } catch (error) {
        setErrorMessage(error);
      } finally {
        setLoading(false);
      }
    };

    fetchGenres();
  }, []);

  useEffect(() => {
    if (selectedMovie !== null) {
      const fetchMovieData = async () => {
        try {
          const response = await axios.get(
            getMovieDetailsURL(selectedMovie.id)
          );
          setSelectedMovieVideoId(response.data.videos.results[0]?.key);
        } catch (error) {
          setErrorMessage(error);
        }
      };

      fetchMovieData();
    }
  }, [selectedMovie]);





  useEffect(() => {
    const defaultRating = ratingValues[0] === 0 && ratingValues[1] === 10;

    if (selectedGenres.length > 0 && !defaultRating) {
      getData(
        getRatingAndGenresURL(ratingValues[0], ratingValues[1], selectedGenres)
      );
      setTitle("Selected genres and rating");
    } else if (selectedGenres.length > 0) {
      getData(getGenresSelectedURL(selectedGenres));
      setTitle("Selected genres");
    } else if (!defaultRating) {
      getData(getScoreSelectedURL(ratingValues[0], ratingValues[1]));
      setTitle("Selected by rating");
    } else {
      getData(API);
      setTitle("Popular movies");
    }
  }, [ratingValues, selectedGenres]);





  const handleOnSubmit = async (e) => {
    setLoading(true);
    setSelectedGenres([])

    if (e.target.value) {
      getData(SEARCH_API + e.target.value);
      setTitle(`Movies include "${e.target.value}"`);
    } else {
      getData(API);
      setTitle("Popular movies");
    }
  };

 


  const _ = require("lodash");
  const debounced = _.debounce(handleOnSubmit, 2000, { maxWait: 2000 });

  return (
    <div className={s.container}>

      <Header handleOnChange={debounced}/>

      <div className={s.flex_wrapper}>
        <RangeSlider
          setRatingValues={setRatingValues}
          ratingValues={ratingValues}
        />
        <GenresSelection
          genres={genres}
          setSelectedGenres={setSelectedGenres}
          selectedGenres={selectedGenres}
        />
      </div>

      {loading ? (
        <Loader />
      ) : errorMessage ? (
        <div className={s.error_wrapper}>
          <p>Some Error Occurred</p>
        </div>
      ) : data.length === 0 ? (
        <div className={s.error_wrapper}>
          <p>No movies found</p>
        </div>
      ) : (
        <>
          <Movies
            title={title}
            genres={genres}
            movies={currentMovies}
            selectedMovie={selectedMovie}
            setSelectedMovie={setSelectedMovie}
            selectedMovieVideoId={selectedMovieVideoId}
          ></Movies>

          <div className={s.pagination_wrapper}>
            <Pagination
              moviesPerPage={moviesPerPage}
              totalMovies={data.length}
              paginate={paginate}
              currentPage={currentPage}
            />
          </div>
        </>
      )}
    </div>
  );
}

export default Main;
