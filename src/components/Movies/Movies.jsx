import Movie from "../Movie/Movie";
import s from "./style.module.css";
import { useState } from "react";
import Modal from "../Modal/Modal";
import ModalContent from "../ModalContent/ModalContent";

const Movies = (props) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const imageUrl = "https://image.tmdb.org/t/p/w500";

  return (
    <>
      <p className={s.title}>{props.title}</p>

      <div className={s.movies_wrapper}>
        {props.movies?.map((movie) => (
          <Movie
            {...movie}
            result={movie}
            openModal={openModal}
            setSelectedMovie={props.setSelectedMovie}
            image={
              movie.backdrop_path
                ? imageUrl + movie.backdrop_path
                : "https://northernvirginiamag.com/wp-content/uploads/2020/08/movie-popcorn.jpg"
            }
            key={movie.id}
          />
        ))}

        <Modal
          show={isModalOpen}
          closeModal={closeModal}
          selectedMovie={props.selectedMovie}
        >
          <ModalContent
            selectedMovie={props.selectedMovie}
            selectedMovieVideoId={props.selectedMovieVideoId}
            genres={props.genres}
          />
        </Modal>
      </div>
    </>
  );
};

export default Movies;
