import { Range } from "react-range";
import s from "./style.module.css";

const RangeSlider = (props) => {


  return (
    <div className={s.range_wrapper}>
      <div className={s.title}>Select by rating: </div>

      <div>
        <Range
          min={0}
          max={10}
          step={1}
          values={props.ratingValues}
          onChange={props.setRatingValues}
          renderTrack={({ props, children }) => (
            <div {...props} className={s.range_track}>
              {children}
            </div>
          )}
          renderThumb={({ props }) => (
            <div {...props} className={s.range_thumb} />
          )}
        />
        <div className={s.slider_values}>
          {[...Array(11).keys()].map((num) => (
            <div key={num}>{num}</div>
          ))}
        </div>
        <div className={s.text}>
          Selected Range: {props.ratingValues[0]} - {props.ratingValues[1]}
        </div>
      </div>
    </div>
  );
};

export default RangeSlider;
