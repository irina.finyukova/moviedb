import s from "./style.module.css";

const Loader = () => {

  return (
    <div className={s.loader_wrapper}>
    <div class={s.loader}></div>
    </div>

  );
};

export default Loader;
