import s from "./style.module.css";
import cn from "classnames";

const Vote = (props) => {
  const setVoteColor = (vote_average) => {
    if (vote_average >= 8) {
      return s.green;
    } else if (vote_average >= 7) {
      return s.orange_green;
    } else if (vote_average >= 6) {
      return s.orange;
    } else {
      return s.red;
    }
  };

  return (
    <div className={cn(s.vote, setVoteColor(props.scores))}>
            {props.scores.toFixed(1)}
    </div>
  );
};

export default Vote;
