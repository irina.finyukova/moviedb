import s from "./style.module.css";

const GenresSelection = (props) => {
  
  const toggleGenre = (id) => {
    props.setSelectedGenres(prevSelected =>
      prevSelected.includes(id)
        ? prevSelected.filter(genreId => genreId !== id)
        : [...prevSelected, id]
    );
  };


  return (
    <div className={s.genres_wrapper}>
      <div className={s.title}>Select by genre: </div>
      <div className={s.genre_tags}>
         {props.genres.map((el) => (
        <div
          key={el.id}
          className={`${s.genre_tag} ${props?.selectedGenres.includes(el.id) ? s.active : ''}`}
          onClick={() => toggleGenre(el.id)}
        >
          {el.name}
        </div>
      ))}
      </div>
     
    </div>
  );
};

export default GenresSelection;