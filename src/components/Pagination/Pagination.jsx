import s from "./style.module.css";
import cn from "classnames";

const Pagination = ({ moviesPerPage, totalMovies, paginate, currentPage }) => {
  
  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(totalMovies / moviesPerPage); i++) {
    pageNumbers.push(i);
  }

  const setPageColor = (number) => {
    if (currentPage === number) {
     return s.active
    }
  };

  return (
    <div className={s.pagination}>
      {pageNumbers.map((number) => (
        <ul key={number}>
          <li key={number} className={s.page_item}>
            <button onClick={() => paginate(number)} className={cn(s.page_link, setPageColor(number))}>
              {number}
            </button>
          </li>
        </ul>
      ))}
    </div>
  );
};

export default Pagination;


