import s from "./style.module.css";
import Vote from "../Vote/Vote";

const Movie = (props) => {
  let movieScore = props.vote_average;

  const handleClick = (e) => {
    e.stopPropagation();
    props.setSelectedMovie(props);
    props.openModal(true);
  };

  return (
    <>
      <div className={s.movie_item} key={props.id} onClick={handleClick}>
        <div className={s.title}>
          <div className={s.text}>{props.title}</div>

          <div className={s.vote_wrapper}>
            <Vote scores={movieScore} />
          </div>
          
        </div>
        <img className={s.image} src={props.image} alt="movie_image" />
      </div>
    </>
  );
};

export default Movie;
