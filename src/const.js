export const API_KEY = process.env.REACT_APP_API_KEY;
export const API = `https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=${API_KEY}`;

export const SEARCH_API = `https://api.themoviedb.org/3/search/movie?&api_key=${API_KEY}&query=`;

export const GENRES_API = `https://api.themoviedb.org/3/genre/movie/list?api_key=${API_KEY}&language=en`;

export const getMovieDetailsURL = (movieId) => {
  return `http://api.themoviedb.org/3/movie/${movieId}?api_key=${API_KEY}&append_to_response=videos`;
};

export const getGenresSelectedURL = (selectedGenres) => {
  return `https://api.themoviedb.org/3/discover/movie?api_key=${API_KEY}&with_genres=${selectedGenres}`;
};

export const getScoreSelectedURL = (minRating, maxRating) => {
  return `https://api.themoviedb.org/3/discover/movie?api_key=${API_KEY}&language=en-US&sort_by=vote_average.desc&vote_count.gte=1000&vote_average.gte=${minRating}&vote_average.lte=${maxRating}`;
};


export const getRatingAndGenresURL = (minRating, maxRating, genreId) => {
  return `https://api.themoviedb.org/3/discover/movie?api_key=${API_KEY}&language=en-US&sort_by=vote_average.desc&vote_count.gte=1000&vote_average.gte=${minRating}&vote_average.lte=${maxRating}&with_genres=${genreId}`;
};


